# Install Argo CD

[..](./README.md)

Set up ArgoCD that it can install cluster components. After installation ArgoCD is capable to keep itself updated.

```bash
# helm repo
helm repo add argo https://argoproj.github.io/argo-helm

# namespace
kubectl create namespace argocd

# install
# helm install argocd argo/argo-cd -n argocd -f cluster/argoCD/values.yaml
# HUOM! nyt versio 1.4, joka käyttä Helm v.2 -> ei toimi!
helm install argocd argo/argo-cd -n argocd
# versio 1.5 päivittää Helmin versioon 3.
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v1.5.0/manifests/install.yaml

# wait installation finishes
watch kubectl get all -n argocd
```

### CLI

https://argoproj.github.io/argo-cd/cli_installation/

```bash
# version
VERSION=$(curl --silent "https://api.github.com/repos/argoproj/argo-cd/releases/latest" | grep '"tag_name"' | sed -E 's/.*"([^"]+)".*/\1/')

# install
sudo curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/$VERSION/argocd-linux-amd64

# Make the argocd CLI executable:
sudo chmod +x /usr/local/bin/argocd
```

Login to api

```bash
# get argocd-server ip address
kubectl get svc -n argocd | grep argocd-server

# get password
kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2

# username = admin
argocd login <service/argocd-server IP address!!!>

# update password
argocd account update-password
```

<!--
### Git creditentials

TODO: parempi tapa!

```bash
argocd repocreds add git@bitbucket.org:teammoilanen/cluster-deployment.git --ssh-private-key-path ~/.ssh/jenkins_id_rsa
``` -->
