# Kubernetes cluster bootstrap

The hard part of Kubernetes is to choose and make work of diffent pieces need to have for archieving functionality of cluster. There is no need to thing that big cloud providers is only way to go cloud native applications. Minimal cluster has only 1 node. And you can begin with that. You can scale cluster later and use your setup in big scale.

This project begins when you have cluster installed and `kubeclt` CLI is usable.

## Why?

- Development in cloud native way. This means `Kubernetes`, `microservices`, `continuos development` and `Git Ops`.
- Use minimum amount of resources.
- Vendor lock free implementation. You can drive your cluster anywhere. Even in `Raspbery Pi`es.
- Open source. No lisence hassle.
- Easy cluster bootstrap. All nessessary parts will be included. `CI pipeline` is optional. So you can have separate production cluster without `Jenkins` and `Nexus`.
- Keep everything as simple as it can be.

## Requirements

1. Some `Kubernetes` implementation istalled. [Guide for single node K3s installtion](./install-K3s-single-node.md)
1. Helm 3 [Guide](./install-Helm-3.md)
1. OpenEBS installed to cluster. [Guide](./install-OpenEBS.md)
1. Argo CD installed. [Guide](./install-ArgoCD.md)
   1. Login to `ArgoCD CLI` (last part of installtion guide)
   1. Change password / users in `ArgoCD CLI`

Clone repository

```bash
git clone https://bitbucket.org/teammoilanen/kubernetes-bootstrap.git
```

and

- place it somewhere in your GIT server
- give creditentials for ArgoCD to that repo
- set up disks for OpenEBS
- set up in
  - url of cluster
  - acme account

## Use `ArgoCD CLI` to bootstrap cluster

```bash
argocd app create cluster-apps \
    --dest-server https://kubernetes.default.svc \
    --dest-namespace default \
    --repo https://bitbucket.org/teammoilanen/kubernetes-bootstrap.git \
    --path cluster-apps

argocd app sync cluster-apps

# sync traefik
argocd app sync traefik cert-manager

# wait to be ready
watch kubectl get all -n kube-system
# and
watch kubectl get all -n cert-manager

# sync monitors
argocd app sync monitors
```

After all pods are ready

```bash
watch kubectl get pods -A
```

you are able to access all dashboards.

[Traefik](http://traefik2.dev.12thing.com)

## Install CI pipeline to cluster (optional)

```bash
# sync
argocd app sync ci
```

After all pods are ready

```bash
watch kubectl get pods -A
```

there is

- Jenkins
- Nexus

available.

Install demo app for monitors root.

- oma repo, Argo CD CLI
- Jenkins,
  - asenna repo ja buildaa
- ArgoCD sync

goto [http://monitors.dev.12thing.com](http://monitors.dev.12thing.com)

## Now you are ready to set up anyhting else

- services needed to run your applications in cluster
- your applications

## TODO

- Multi node cluster
- HA cluster set up
- Harden security
- Use other than Let's Encrypt SSL certificates
