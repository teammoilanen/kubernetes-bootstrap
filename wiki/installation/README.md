# Installation

## DNS

You need public address for cluster. Point selected ulr to you cluster.

---

## Kubernetes

Uninstall

```bash
/usr/local/bin/k3s-uninstall.sh
```

### HA

TBD

### Multi node

TBD

### Single node

#### Install k3s

**TODO:** There is possible security hole with this installation because of `write-kubeconfig-mode 644`.

Tested version of k3s: `v1.17.0+k3s.1`

```bash
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.0.0 sh -s - --write-kubeconfig-mode 644  --no-deploy traefik
```

Or latest version

```bash
curl -sfL https://get.k3s.io | sh -s - --write-kubeconfig-mode 644 --no-deploy traefik
```

- We don't want to deploy `traefik` because it is version 1.7. We use v2 because of middleware support to authenticate cluster dashboards. Also `Ignress` is dead, so change it to `IngressRoute` (default in Traefik 2)
- K3s has default `write-kubeconfig-mode` 600 and some commands needs `sudo` to work.

Wait until installation finishes

```bash
$ watch kubectl get pods -A

NAMESPACE     NAME                                      READY   STATUS    RESTARTS   AGE
kube-system   local-path-provisioner-58fb86bdfd-ng5lq   1/1     Running   0          46s
kube-system   metrics-server-6d684c7b5-7jxxp            1/1     Running   0          46s
kube-system   coredns-d798c9dd-z447s                    1/1     Running   0          46s
```

---

## Helm 3

```bash
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
```

Helm 3 is compatible with Helm 2 but `tiller` is removed. https://helm.sh/docs/faq/#changes-since-helm-2

If you get `Error: Kubernetes cluster unreachable` do following:

```bash
# make helm available to cluster
kubectl config view --raw >~/.kube/config
# install & update repo
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update
```

---

## OpenEBS

### Prerequisites

#### Unmounted hard drive

Hard drive is discovered automatically by `openEBS`. There is possibility to mount spesific folder, but not recommented.

#### iSCSI client

- https://docs.openebs.io/docs/next/prerequisites.html
- https://docs.openebs.io/docs/next/installation.html#set-cluster-admin-user-context-and-rbac

Set cluster-admin user context and RBAC. You propably need to `sudo` these commands.

```bash
kubectl config set-context admin-ctx --cluster=dev-cluster --user=cluster-admin

kubectl config use-context admin-ctx
```

**TODO:** Install `Helm` part with `Argo CD`

Install with Helm v3

```bash
# create missing namespace
kubectl create namespace openebs
# install
helm install openebs stable/openebs --namespace openebs  --version 1.7.0

NAME: openebs
LAST DEPLOYED: Sat Jan 25 02:49:25 2020
NAMESPACE: openebs
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The OpenEBS has been installed. Check its status by running:
$ kubectl get pods -n openebs

For dynamically creating OpenEBS Volumes, you can either create a new StorageClass or
use one of the default storage classes provided by OpenEBS.

Use `kubectl get sc` to see the list of installed OpenEBS StorageClasses. A sample
PVC spec using `openebs-jiva-default` StorageClass is given below:"

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: demo-vol-claim
spec:
  storageClassName: openebs-jiva-default
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5G
---

For more information, please visit http://docs.openebs.io/.

Please note that, OpenEBS uses iSCSI for connecting applications with the
OpenEBS Volumes and your nodes should have the iSCSI initiator installed.
```

https://openebs.io

```bash
# wait until ready
watch kubectl get pods -n openebs

NAME                                           READY   STATUS    RESTARTS   AGE
openebs-localpv-provisioner-67bddc8568-nhzvs   1/1     Running   0          2m52s
openebs-ndm-ngcf5                              1/1     Running   0          2m52s
openebs-provisioner-c68bfd6d4-8pll7            1/1     Running   0          2m52s
openebs-admission-server-889d78f96-lp2br       1/1     Running   0          2m52s
openebs-snapshot-operator-7ffd685677-2nksr     2/2     Running   0          2m52s
openebs-ndm-operator-5db67cd5bb-sxhw6          1/1     Running   1          2m52s
maya-apiserver-7f664b95bb-6svmw                1/1     Running   2          2m52s

# storage classes
kubectl get sc

NAME                        PROVISIONER                                                RECLAIMPOLICY   VOLUMEBINDINGMODE      ALLOWVOLUMEEXPANSION   AGE
local-path (default)        rancher.io/local-path                                      Delete          WaitForFirstConsumer   false                  64m
openebs-jiva-default        openebs.io/provisioner-iscsi                               Delete          Immediate              false                  2m46s
openebs-snapshot-promoter   volumesnapshot.external-storage.k8s.io/snapshot-promoter   Delete          Immediate              false                  2m46s
openebs-hostpath            openebs.io/local                                           Delete          WaitForFirstConsumer   false                  2m46s
openebs-device              openebs.io/local                                           Delete          WaitForFirstConsumer   false                  2m46s
```

> **NOTE!** For larger than single node clusters, https://docs.openebs.io/docs/next/installation.html#example-nodeselector-yaml

How `openEBS` works: https://docs.openebs.io/docs/next/cstor.html#relationship-between-cstor-volumes-and-cstor-pools
![](../pictures/pvcspc.png)

#### Verify Block Device CRs

In my configuration is no block devices because of `Block devices that is discovered on the node with two exceptions: ... - The disks that are already mounted in the node`.

```bash
kubectl get blockdevice -n openebs
# if no unmounted hard drive
No resources found in openebs namespace.
# unmounted hard drive found
NAME                                           NODENAME    SIZE           CLAIMSTATE   STATUS   AGE
blockdevice-7f946a4b974f55395b71025a7c3b5e98   dev-cloud   500107862016   Unclaimed    Active   32m
```

Create StoragePool

```bash
$ kubectl apply -f openEBS/cStor-pool.yml

# check
$ kubectl get spc

NAME              AGE
cstor-disk-pool   12s

# and
$ kubectl get blockdevice -n openebs

NAME                                           NODENAME    SIZE           CLAIMSTATE   STATUS   AGE
blockdevice-7f946a4b974f55395b71025a7c3b5e98   dev-cloud   500107862016   Claimed      Active   6d11h

# and
$ kubectl get csp

NAME              ALLOCATED   FREE   CAPACITY   STATUS    TYPE      AGE
cstor-disk-p3th   202K        464G   464G       Healthy   striped   5m51s
# and
$ kubectl get pod -n openebs | grep cstor-disk-pool
cstor-disk-pool-0bpr-8c594647c-8ct8x           3/3     Running   0          108s
```

---

## Argo CD

![argo-architechture](../pictures/argocd_architecture.png)

https://github.com/argoproj/argo-cd/blob/master/docs/getting_started.md

Install UI

```bash
 helm repo add argo https://argoproj.github.io/argo-helm

helm install argocd argo/argo-cd -n cd-pipeline -f cd\ pipeline/argocd/values.yaml
```

Install CLI

https://argoproj.github.io/argo-cd/cli_installation/

```bash
# version
VERSION=$(curl --silent "https://api.github.com/repos/argoproj/argo-cd/releases/latest" | grep '"tag_name"' | sed -E 's/.*"([^"]+)".*/\1/')

# install
sudo curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/$VERSION/argocd-linux-amd64

# Make the argocd CLI executable:
sudo chmod +x /usr/local/bin/argocd
```

Login to api

```bash
# get argocd-server ip address
kubectl get svc -n cd-pipeline

# password
kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2

# username = admin
argocd login <service/argocd-server IP address!!!>

# update password
argocd account update-password
```

## Create application in Argo CD

1. connect to repo. You should user `ssh` keys to access private repos because of bug in `Argo CD` (26.2.2020)

## Argo CD is unable to connect to my cluster, how do I troubleshoot it?

Use the following steps to reconstruct configured cluster config and connect to your cluster manually using kubectl:

```bash
kubectl exec -it -n cd-pipeline <argocd-pod-name> bash # ssh into any argocd server pod
argocd-util kubeconfig https://<cluster-url> /tmp/config --namespace argocd # generate your cluster config
KUBECONFIG=/tmp/config kubectl get pods # test connection manually
```

---

## Bootstrap rest of the cluster

Use Argo CD for rest of the cluster.

1. Use file ... to set up repo & deploy repo of this projct to argo
1. run magic command to bootstrap rest of cluster

```bash
argocd login admin <CLUSTER>
argocd install ....
```

---

## CI-pipeline [OPTIONAL]

If you wish to have ability to build artifacts with `Jenkins` & save them to local `Nexus` repository, install this part as well.

1. open argocd ui, and install project `CI pipeline`.
1. enjoy
