# Install Helm v3

[..](./README.md)

```bash
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
```

Helm 3 is compatible with Helm 2 but `tiller` is removed. https://helm.sh/docs/faq/#changes-since-helm-2

If you get `Error: Kubernetes cluster unreachable` do following:

```bash
# make helm available to cluster
kubectl config view --raw >~/.kube/config
# install & update repo
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update
```
