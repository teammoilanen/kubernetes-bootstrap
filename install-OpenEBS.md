# Install OpenEBS

Storage need to be think also in cloud native way. If disk is dead, it is dead and need to be replaced. This set up uses `OpenEBS` solution.

- Uses un mounted disks in cluster
- Backup support also for snapshots of disks. Can use `S3` for backup disk.
- Restore backups easy.
- Multiple disks, even in different locations. `OpenEBS` keep them in sync.
- With multi deployment one disk lost is easy to repair. Just replace it with working disk and `OpenEBS` sync it's data from working disks.

## Prerequisites

### Unmounted hard drive

Hard drive is discovered automatically by `OpenEBS`. There is possibility to mount spesific folder, but not recommented.

### iSCSI client

- https://docs.openebs.io/docs/next/prerequisites.html
- https://docs.openebs.io/docs/next/installation.html#set-cluster-admin-user-context-and-rbac

**NOTE!!**: Ei välttämättä tarvi. Kokeile ensin ilman,

Set cluster-admin user context and RBAC. You propably need to `sudo` these commands.

```bash
kubectl config set-context admin-ctx --cluster=dev-cluster --user=cluster-admin

kubectl config use-context admin-ctx
```

Install with Helm v3

```bash
# create missing namespace
kubectl create namespace openebs
# install
helm install openebs stable/openebs --namespace openebs  --version 1.7.0

NAME: openebs
LAST DEPLOYED: Sat Jan 25 02:49:25 2020
NAMESPACE: openebs
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The OpenEBS has been installed. Check its status by running:
$ kubectl get pods -n openebs

For dynamically creating OpenEBS Volumes, you can either create a new StorageClass or
use one of the default storage classes provided by OpenEBS.

Use `kubectl get sc` to see the list of installed OpenEBS StorageClasses. A sample
PVC spec using `openebs-jiva-default` StorageClass is given below:"

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: demo-vol-claim
spec:
  storageClassName: openebs-jiva-default
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5G
---

For more information, please visit http://docs.openebs.io/.

Please note that, OpenEBS uses iSCSI for connecting applications with the
OpenEBS Volumes and your nodes should have the iSCSI initiator installed.
```

https://openebs.io

```bash
# wait until ready
watch kubectl get pods -n openebs

NAME                                           READY   STATUS    RESTARTS   AGE
openebs-localpv-provisioner-67bddc8568-nhzvs   1/1     Running   0          2m52s
openebs-ndm-ngcf5                              1/1     Running   0          2m52s
openebs-provisioner-c68bfd6d4-8pll7            1/1     Running   0          2m52s
openebs-admission-server-889d78f96-lp2br       1/1     Running   0          2m52s
openebs-snapshot-operator-7ffd685677-2nksr     2/2     Running   0          2m52s
openebs-ndm-operator-5db67cd5bb-sxhw6          1/1     Running   1          2m52s
maya-apiserver-7f664b95bb-6svmw                1/1     Running   2          2m52s

# storage classes
kubectl get sc

NAME                        PROVISIONER                                                RECLAIMPOLICY   VOLUMEBINDINGMODE      ALLOWVOLUMEEXPANSION   AGE
local-path (default)        rancher.io/local-path                                      Delete          WaitForFirstConsumer   false                  64m
openebs-jiva-default        openebs.io/provisioner-iscsi                               Delete          Immediate              false                  2m46s
openebs-snapshot-promoter   volumesnapshot.external-storage.k8s.io/snapshot-promoter   Delete          Immediate              false                  2m46s
openebs-hostpath            openebs.io/local                                           Delete          WaitForFirstConsumer   false                  2m46s
openebs-device              openebs.io/local                                           Delete          WaitForFirstConsumer   false                  2m46s
```

> **NOTE!** For larger than single node clusters, https://docs.openebs.io/docs/next/installation.html#example-nodeselector-yaml

How `openEBS` works: https://docs.openebs.io/docs/next/cstor.html#relationship-between-cstor-volumes-and-cstor-pools
![](wiki/pictures/pvcspc.png)

#### Verify Block Device CRs

In my configuration is no block devices because of `Block devices that is discovered on the node with two exceptions: ... - The disks that are already mounted in the node`.

```bash
kubectl get blockdevice -n openebs
# if no unmounted hard drive
No resources found in openebs namespace.
# unmounted hard drive found
NAME                                           NODENAME    SIZE           CLAIMSTATE   STATUS   AGE
blockdevice-7f946a4b974f55395b71025a7c3b5e98   dev-cloud   500107862016   Unclaimed    Active   32m
```

Create StoragePool

```bash
$ kubectl apply -f openEBS/cStor-pool.yml

# check
$ kubectl get spc

NAME              AGE
cstor-disk-pool   12s

# and
$ kubectl get blockdevice -n openebs

NAME                                           NODENAME    SIZE           CLAIMSTATE   STATUS   AGE
blockdevice-7f946a4b974f55395b71025a7c3b5e98   dev-cloud   500107862016   Claimed      Active   6d11h

# and
$ kubectl get csp

NAME              ALLOCATED   FREE   CAPACITY   STATUS    TYPE      AGE
cstor-disk-p3th   202K        464G   464G       Healthy   striped   5m51s
# and
$ kubectl get pod -n openebs | grep cstor-disk-pool
cstor-disk-pool-0bpr-8c594647c-8ct8x           3/3     Running   0          108s
```
