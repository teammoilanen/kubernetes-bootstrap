# Single node `K3s` installtion

[..](./README.md)

For reset installation: Uninstall k3s

```bash
/usr/local/bin/k3s-uninstall.sh
```

Note write mode about cube config.

Tested version of k3s: `v1.17.0+k3s.1`

```bash
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.17.0+k3s.1 sh -s - --write-kubeconfig-mode 644  --no-deploy traefik
```

Or latest version

```bash
curl -sfL https://get.k3s.io | sh -s - --write-kubeconfig-mode 644 --no-deploy traefik
```

- We don't want to deploy `traefik` because it is version 1.7. We use v2 because of middleware support to authenticate cluster dashboards.
- K3s has default `write-kubeconfig-mode` 600 and some commands needs `sudo` to work.

Wait until installation finishes

```bash
$ watch kubectl get pods -A

NAMESPACE     NAME                                      READY   STATUS    RESTARTS   AGE
kube-system   local-path-provisioner-58fb86bdfd-ng5lq   1/1     Running   0          46s
kube-system   metrics-server-6d684c7b5-7jxxp            1/1     Running   0          46s
kube-system   coredns-d798c9dd-z447s                    1/1     Running   0          46s
```
