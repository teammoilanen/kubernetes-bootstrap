# Polaris

[Helm repository](https://charts.fairwindsops.com/stable) is not found for [Polaris](https://github.com/FairwindsOps/polaris). Installation is done by `yaml`.

There is 3 services:

- [x] [dasboard](https://github.com/FairwindsOps/polaris#dashboard)
- [ ] [validating webhook](https://github.com/FairwindsOps/polaris#webhook)
- [ ] [command line tool](https://github.com/FairwindsOps/polaris#cli)

## Dashboard

https://github.com/fairwindsops/polaris/releases/

Version 0.6.0

## Changes to original yaml

1. Namespace is changed to monitors.
